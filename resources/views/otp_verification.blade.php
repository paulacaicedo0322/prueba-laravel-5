<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <h1>Verificar correo</h1>
    <form action="{{ route('verifyotp') }}" method="post">
        @csrf
        <div class="form-group">
            <label for="" class="col-form-label">Ingrese el codigo</label>
            <input type="number" class="form-control" name="token">
        </div>
        <button type="submit" class="btn btn-primary">Verificar</button>
    </form>

</body>
</html>