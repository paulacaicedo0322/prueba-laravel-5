<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Verifytoken;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function Useractivation(Request $request){
        $verifycoursetoken = $request->token;
        $verifycoursetoken = Verifytoken::where('token', $verifycoursetoken)->first();
        if ($verifycoursetoken){
            $verifycoursetoken->is_activated = 1;
            $verifycoursetoken->save();

            $user = User::where('email', $verifycoursetoken->email)->first();
            $user->is_activated = 1;
            $user->save();

        $getting_token = Verifytoken::where('token', $verifycoursetoken->token)->first();
        $getting_token->delete();

        return redirect('/home')->with('activated', 'Su cuenta fue activada');

        }else{
            return redirect('/verify-account');
        }
    }

    public function verifyaccount(Request $request){
        return view('otp_verification');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $get_user = User::where('email', auth()->user()->email)->first();

        if($get_user->is_activated == 1){
            return view('home');
        }else{
            return redirect('/verify-account');
        }
    }
}
